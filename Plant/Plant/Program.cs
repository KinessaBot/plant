﻿using System;

namespace Plant
{
    class Program
    {
        static void Main(string[] args)
        {
            Plant carrot = new Plant("eggplant");
            Herbivore rabbit = new Herbivore("hedgehog");
            Carnivore snake = new Carnivore("bear");
            carrot.Grow += ShowMessage;
            carrot.Grows();
            carrot.HasGrown();
            if (carrot.Grown)
            {
                rabbit.EatPlant += ShowMessage;
                rabbit.Eating(carrot.Kind);
            }
            if (rabbit.Eat)
            {
                snake.EatHerbivore += ShowMessage;
                snake.Eating(rabbit.Eater);
            }
            Console.ReadLine();
        }

        private static void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
