﻿namespace Plant
{
    class Herbivore
    {
        public delegate void HerbivoreHandler(string mes);
        public event HerbivoreHandler EatPlant;

        public string Eater { set; get; }
        public bool Eat { get; private set; }

        public Herbivore(string eater)
        {
            Eat = false;
            Eater = eater;
        }

        public void Eating(string Plant)
        {
            Eat = true;
            if (EatPlant != null)
                EatPlant($"{Plant} has been eaten by {Eater}");
        }
    }
}
