﻿namespace Plant
{
    class Plant
    {
        public delegate void PlantHandler(string mes);
        public event PlantHandler Grow;

        public string Kind { set; get; }
        public bool Grown { get; private set; }

        public Plant(string kind)
        {
            Grown = false;
            Kind = kind;
        }

        public void Grows()
        {
            if (Grow != null)
                Grow($"{Kind} grows");
        }

        public void HasGrown()
        {
            Grown = true;
            if (Grow != null)
                Grow($"{Kind} has grown");
        }
    }
}
